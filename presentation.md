title: Coopdevs - Optimització de processos empresarials
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
.bottom-bar[
  {{title}}
]

---

# Àmbits d'actuació

### Audit situació
### Anàlisis de necessitats
### Disseny de la solució
### Guia d'execució
### Execució
